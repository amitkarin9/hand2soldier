import React, { Component} from 'react';
import Navbar from './components/Navbar';
import {BrowserRouter,Route,Routes} from 'react-router-dom';
import Home from './components/Home';
import MainPage from './components/MainPage';
import Requests from './components/Requests';
import Signin from './components/Signin';
import Profile from './components/Profile';
import Signup from './components/Signup';

class App extends Component {
  LoginRequest() {
    // Simple POST request with a JSON body using fetch
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ title: 'React POST Request Example' })
    };
    fetch('/login', requestOptions)
        .then(response => response.json())
        .then(data => this.setState({ postId: data.id }));
    }


  render() {
    return (
      <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Routes>
          <Route path="/profile" element={<MainPage/>} />
          <Route exact path="/" element={<Home/>}/>
          <Route path="/requests" element={<Requests/>}/>
          <Route path="/signin" element={<Signin/>}/>
          <Route path="/signup" element={<Signup/>}/>

        </Routes>
      </div>
      </BrowserRouter>
    );
  }
}
export default App;