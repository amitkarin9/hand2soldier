import React, { Component } from 'react';
import FlatList from 'react';
import './Requests.css';
import img1 from '../public/images/add1.png';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

class Requests extends Component {
    render() {
        return (
            <div className="requeste-wrapper">
                <div>
                    <h1>בקשות החיילים</h1>
                </div>
                <ul className='mylist1'>
                    <il className='ul1'>שמלת ערב כחולה</il>
                    <il className='ul1'>מקרר 4 דלתות כסוף</il>
                    <il className='ul1'>ארון הזזה בצבע לבן</il>
                </ul>
                <ul className='mylist1'>
                    <il className='ul1'>מכונת כביסה</il>
                    <il className='ul1'>שידת איפור עם מגירות</il>
                    <il className='ul1'>מיטה זוגית</il>
                </ul>
                <ul className='mylist1'>
                    <il className='ul1'>ספה סלונית</il>
                    <il className='ul1'>כורסה בצבע אפור</il>
                    <il className='ul1'>נעלי ספורט מידה 39 לבנות</il>
                </ul>
                <IconButton color="primary">
                <img src={img1} width={100} style={{ cursor: "pointer", justifyContent:'center'}} />
                </IconButton>

            </div>
        );
    }
}
export default Requests;


