import Item1 from '../../public/images/sapa.jpg'
import Item2 from '../../public/images/table.jpg'
import Item4 from '../../public/images/bed.jpg'
import Item5 from '../../public/images/fridge.jpg'
import Item6 from '../../public/images/tv.jpg'
import Item3 from '../../public/images/oven.jpg'




const initState = {
    items: [
        {id:1,title:'ספה', desc: "ספה סלונית וכורסא מעץ מלא, למסירה   בתל אביב, יוסי 0543625438", img:Item1},
        {id:2,title:'שולחן אוכל', desc: "שולחן פינת אוכל 150*80 נפתח לעוד 70 ס'מ. ועוד 6 כסאות. למסירה מאזור ירושלים, אביב 05443162789",img: Item2},
        {id:3,title:'תנור', desc: "תנור גודל 50 ס'מ, לבן עובד מעולה. למסירה מנתניה, יעקב 0587651324",img: Item3},
        {id:4,title:'מיטה זוגית', desc: "מיטה זוגית + מזרון 160*200, מאזור נתניה. שובל 05442318765",img:Item4},
        {id:5,title:'מקרר', desc: "מקרר במצב מעולה למסירה מאזור פתח תקווה. בר 0554238790", price:160,img: Item5},
        {id:6,title:'מזנון', desc: "מזנון מודרני בצבע לבן, מטר וחצי על 40 ס'מ. למסירה מאזור הוד השרון. רז 0548763290    ",price:90,img: Item6}
    ],
    addedItems:[],
    total: 0

}
const cartReducer= (state = initState,action)=>{
    
    return state;

}
export default cartReducer;