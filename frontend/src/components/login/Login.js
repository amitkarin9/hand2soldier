import React from 'react';
import { useState } from 'react';
import './Login.css';
import { useNavigate } from "react-router-dom";
import axios from 'axios';

export default function Login({updateIsLogin}) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  let navigate = useNavigate();
  const routeChange = () =>{ 
    let path = '/signup'; 
    navigate(path);
  }
  const changePassword = (newPass) => {
    setPassword(newPass);
  }

  const changeEmail = (newEmail) => {
    setEmail(newEmail);
  }
  const login = () => {
    axios.post('http://localhost:5000/login?email='+email+'&password='+password).then((res) => {
      console.log(res.data)
      updateIsLogin(true);
  }).catch((error) => {
      console.log(error)
  });
  }
  return(
    <div className="login-wrapper">

    <form class="login-form" action="javascript:void(0);">
      <h1>טוב שבאת הביתה </h1>
      <div class="form-input-material">
        <label for="email">מייל</label>
        <input type="text" name="email" id="email" placeholder="yourmail@email.com" autocomplete="off" class="form-control-material"  onChange={(e) => changeEmail(e.target.value)} required />   
      </div>
      <div class="form-input-material">
        <label for="password">סיסמא</label>
        <input type="password" name="password" id="password" placeholder="הקלדת סיסמא " autocomplete="off" class="form-control-material"  onChange={(e) => changePassword(e.target.value)} required />
      </div>
      <br/>
      <button type="submit" variant="contained" class="btn btn-primary btn-ghost" onClick={() => login()}>התחבר</button>
      <div class='signin'><label>עדיין לא רשום ?</label> <a class='signinnow' onClick={routeChange}>הירשם עכשיו</a></div>
    </form>
    </div>
  )
}
