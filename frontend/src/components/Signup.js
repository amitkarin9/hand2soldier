import React from 'react';
import './Signup.css';
import Button from '@mui/material/Button';
import { useNavigate } from "react-router-dom";


export default function Login() {
  function handleSubmit(e) {
    e.preventDefault();
    console.log('You clicked submit.');
  }
  let navigate = useNavigate(); 

  const routeChange = () =>{ 
    let path = '/signup'; 
    navigate(path);
  }

  return(
    <div className="login-wrapper">

    <form onSubmit={handleSubmit} class="login-form" action="javascript:void(0);">
      <center><h1>הרשמה </h1></center>
      <div class="form-input-material">
        <label for="username">מייל</label>
        <input type="text" name="username" id="username" placeholder="yourmail@email.com" autocomplete="off" class="form-control-material" required />   
      </div>
      <div class="form-input-material">
        <label for="password">סיסמא</label>
        <input type="password" name="password" id="password" placeholder="הקלדת סיסמא " autocomplete="off" class="form-control-material" required />
      </div>
      <div class="form-input-material">
        <label for="fname">שם פרטי</label>
        <input type="text" name="fname" id="fname" placeholder="הקלדת שם פרטי " autocomplete="off" class="form-control-material" required />
      </div>
      <div class="form-input-material">
        <label for="lname">שם משפחה</label>
        <input type="text" name="lname" id="lname" placeholder="הקלדת שם משפחה " autocomplete="off" class="form-control-material" required />
      </div>
      <div class="form-input-material">
        <label for="phone">מספר טלפון</label>
        <input type="text" name="phone" id="phone" placeholder="הקלדת מספר טלפון " autocomplete="off" class="form-control-material" required />
      </div>
      <label for="type">סוג</label><br/>
      <input type="radio" id="donor" name="type" value="donor" checked/><label for="donor">תורמ/ת</label>
      <input type="radio" id="soldier" name="type" value="soldier" /><label for="soldier">חייל/ת</label>

      <br/><br/>
      <button type="submit" variant="contained" class="btn btn-primary btn-ghost">הירשם</button>
    </form>
    </div>
  )
}
