import './Profile.css';
import axios from 'axios'
import { commenter } from 'stylis';

export default function showProfile(){
        return (
          <center>
          <div class="container">
          <div class="row gutters">
          <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
          <div class="card h-100">
            <div class="card-body">
              <div class="account-settings">
                <div class="user-profile">
                  <div class="user-avatar">
                  </div>
                  <h5 class="user-name">איציק כהן</h5>
                  <h6 class="user-email">ItzikTheKingOfKings@gmail.com</h6>
                </div>
                <div class="about">
                  <h5>About</h5>
                  <p>אוהב את החיילים שלנו. תורם באהבה</p>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
          <div class="card h-100">
            <div class="card-body">
              <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <h6 class="mb-2 text-primary">פרטים אישיים</h6>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                  <div class="form-group">
                    <label for="phone">0524454211</label>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                </div>
              </div>
              <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                </div>
              </div>
            </div>
          </div>
          </div>
          </div>
          </div>
          </center>
        )
    }
