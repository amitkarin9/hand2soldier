import React from 'react';
import { useState } from 'react';
import Login from './login/Login';
import Profile from './Profile';
export default function MainPage() {
  const [isLogin, setIsLogin] = useState(false);
  function updateIsLogin(loginState) {
    setIsLogin(loginState);
  }
  if (!isLogin) {
      return(
    <Login updateIsLogin={updateIsLogin}/>
  );} else {
      return(<Profile/>) ;
  }
}
