import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore } from 'redux';
import cartReducer from './components/reducers/cartReducer';
import { Provider } from 'react-redux';

const store = createStore(cartReducer);


ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

