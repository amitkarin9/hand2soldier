from time import ctime
from flask import Flask, jsonify,request,make_response
import json
import time
import flask
from flask_cors import CORS
from random import randint, randrange

app = Flask(__name__)
database = "server\\db.json"
CORS(app, supports_credentials=True)
@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/login' , methods=["post"])
def check_login():
    email = request.args.get('email')
    password = request.args.get('password')
    print(email)
    print(password)
    f = open(database, encoding="utf8")
    data = json.load(f)

    for user in data["users"]:
        if user["Email"]==email and int(user["Password"])==int(password):
            print(user)
            resp = flask.Response(json.dumps(user), status=200)
            #resp.headers['Access-Control-Allow-Origin'] = ['*']
            return resp

    f.close()
    return make_response(jsonify({"error":"no user found"}),403)
    
@app.route('/signin' , methods=["post"])
def sign_in():
    data = request.data
    signing = json.loads(data)
    id = randint(100000000, 999999999)
    new_user = {"ID": id}
    new_user.update(signing)
    f = open(database, encoding="utf8")
    users = json.load(f)
    f.close()
    users["users"].append(new_user)
    with open(database, 'w', encoding="utf8") as outfile:
        json.dump(users, outfile)
    outfile.close()
    return make_response(jsonify(204))
    
@app.route('/addItem' , methods=["put"])
def add_item():
    data = json.loads(request.data)
    id = randint(100000000, 999999999)
    new_item = {"ID": id}
    new_item.update(data)
    f = open(database, encoding="utf8")
    items = json.load(f)
    f.close()
    items["items"].append(new_item)
    with open(database, 'w', encoding="utf8") as outfile:
        json.dump(items, outfile)
    outfile.close()
    return make_response(jsonify(204))

@app.route('/getItems',methods=["get"])
def get_items():
    f = open(database, encoding="utf8")
    data = json.load(f)
    return make_response(jsonify(data["users"]),200)

if __name__ == '__main__' : 
    app.run()
