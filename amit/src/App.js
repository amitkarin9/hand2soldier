import React, { Component } from 'react';
import Navbar from './components/Navbar';
import {BrowserRouter,Route,Routes} from 'react-router-dom';
import Home from './components/Home';
import Requests from './components/Requests';
import Signin from './components/Signin';
import Profile from './components/Profile';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Routes>
          <Route path="/profile" element={<Profile/>} />
          <Route exact path="/" element={<Home/>}/>
          <Route path="/requests" element={<Requests/>}/>
          <Route path="/signin" element={<Signin/>}/>
        </Routes>
      </div>
      </BrowserRouter>
    );
  }
}
export default App;