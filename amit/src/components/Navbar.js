import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
class Navbar extends Component {
    render(){
        return(
            <nav className="navBar">
                <div className='rightNav'>
                    <li><NavLink to="/profile">פרופיל</NavLink></li>
                    <li><NavLink exact to="/">חנות</NavLink></li>
                    <li><NavLink to="/requests/">בקשות</NavLink></li>
                </div>
                <div className='signin'><NavLink to="/signin/">היכנס</NavLink></div>

            </nav>
        );
    }
}
export default Navbar;