import React from 'react';
import './Login.css';
import Button from '@mui/material/Button';


export default function Login() {
  return(
    <div className="login-wrapper">

    <form class="login-form" action="javascript:void(0);">
      <h1>טוב שבאת הביתה </h1>
      <div class="form-input-material">
        <label for="username">מייל</label>
        <input type="text" name="username" id="username" placeholder="yourmail@email.com" autocomplete="off" class="form-control-material" required />   
      </div>
      <div class="form-input-material">
        <label for="password">סיסמא</label>
        <input type="password" name="password" id="password" placeholder="הקלדת סיסמא " autocomplete="off" class="form-control-material" required />
      </div>
      <br/>
      <button type="submit" variant="contained" class="btn btn-primary btn-ghost">התחבר</button>
      <div class='signin'><label>עדיין לא רשום ?</label> <a class='signinnow'>הירשם עכשיו</a></div>
    </form>
    </div>
  )
}

